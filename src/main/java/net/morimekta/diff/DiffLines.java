package net.morimekta.diff;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Make diff based on lines.
 */
public class DiffLines extends DiffBase {
    private final LinkedList<Change> changeList;

    public DiffLines(String text1, String text2) {
        this(text1, text2, DiffOptions.defaults());
    }
    public DiffLines(String text1, String text2, DiffOptions options) {
        super(options, getDeadline(options));

        this.changeList = makeLineDiff(text1, text2);
    }

    /**
     * @return The full diff including unchanged lines.
     */
    public String fullDiff() {
        StringBuilder builder = new StringBuilder();
        for (Change ch : getChangeList()) {
            builder.append(ch.patchLine())
                   .append("\n");
        }
        return builder.toString();
    }

    /**
     * @return Make a diff description usable by unix <code>patch</code>
     *         program.
     */
    public String patch() {
        StringBuilder builder = new StringBuilder();
        int src_pos = 1, trg_pos = 1;
        LinkedList<Change> changes = new LinkedList<>(getChangeList());
        while (!changes.isEmpty()) {
            if (changes.peekFirst().operation == Operation.EQUAL) {
                changes.pollFirst();
                ++src_pos;
                ++trg_pos;
                continue;
            }
            LinkedList<Change> upcoming = new LinkedList<>();

            int ins = 0;
            int rms = 0;
            while (!changes.isEmpty()) {
                if (changes.peekFirst().operation == Operation.EQUAL) {
                    break;
                }
                Change ch = changes.pollFirst();
                upcoming.add(ch);
                if (ch.operation == Operation.INSERT) {
                    ++ins;
                } else {
                    ++rms;
                }
            }

            builder.append("@@ -")
                   .append(src_pos)
                   .append(',')
                   .append(rms)
                   .append(" +")
                   .append(trg_pos)
                   .append(',')
                   .append(ins)
                   .append(" @@\n");

            for (Change ch : upcoming) {
                builder.append(ch.patchLine())
                       .append("\n");
                if (ch.operation == Operation.INSERT) {
                    ++trg_pos;
                } else {
                    ++src_pos;
                }
            }
        }

        return builder.toString();
    }

    /**
     * Make a display-patch output with extra lines before and after each change.
     *
     * @param before Number of lines to show before each change.
     * @param after Number of lines to show after each cange.
     * @return An easy readable display patch string.
     */
    public String displayPatch(int before, int after) {
        return displayPatch(before, after,
                            "", "",
                            "", "",
                            "", "",
                            "", "", "");
    }

    /**
     * Make a display-patch output with extra lines before and after each change.
     *
     * @param before Number of lines to show before each change.
     * @param after Number of lines to show after each cange.
     * @param beforePatch String to insert before the patch info.
     * @param afterPatch String to insert after the patch info.
     * @param beforeComment String to insert before the patch comment.
     * @param afterComment String to insert after the patch comment.
     * @param beforeEq String to insert before equal line.
     * @param afterEq String to insert after equal line.
     * @param beforeAdd String to insert before added line.
     * @param beforeDel String to insert before deleted line.
     * @param afterChange String to insert after any changed line.
     * @return An easy readable display patch string.
     */
    public String displayPatch(final int before,
                               final int after,
                               // {beforePath}@@ ... @@{afterPatch}{beforeComment} -- ...{afterComment}
                               final @Nonnull String beforePatch,
                               final @Nonnull String afterPatch,
                               final @Nonnull String beforeComment,
                               final @Nonnull String afterComment,
                               // {beforeEq} ...{afterEq}
                               final @Nonnull String beforeEq,
                               final @Nonnull String afterEq,
                               // {beforeAdd}+...{afterChange}
                               final @Nonnull String beforeAdd,
                               // {beforeDel}-...{afterChange}
                               final @Nonnull String beforeDel,
                               final @Nonnull String afterChange) {
        StringBuilder builder = new StringBuilder();

        int skippedLines = -1;
        int sourceLineNo = 0;
        int targetLineNo = 0;

        ArrayList<Change> changes = new ArrayList<>(getChangeList());
        boolean printBefore = false;
        int printAfter = 0;
        for (int i = 0; i < before && i < changes.size(); ++i) {
            if (changes.get(i).operation != Operation.EQUAL) {
                printBefore = true;
                break;
            }
        }

        for (int i = 0; i < changes.size(); ++i) {
            Change ch = changes.get(i);
            switch (ch.operation) {
                case EQUAL: {
                    ++sourceLineNo;
                    ++targetLineNo;
                    {
                        if (!printBefore && printAfter < 1) {
                            // we're not currently printing anything...
                            if (before > 0) {
                                // check set of 'before' lines after current to check for changes.
                                for (int off = 0; off < before && i + off + 1 < changes.size(); ++off) {
                                    if (changes.get(i + off + 1).operation != Operation.EQUAL) {
                                        printBefore = true;
                                        break;
                                    }
                                }
                            }
                            // If not, check if we might be skipping 1 line.
                            if (!printBefore) {
                                if ((before > 0 || after > 0) &&
                                    i > after &&
                                    i < (changes.size() - before - 1) &&
                                    changes.get(i - after - 1).operation != Operation.EQUAL &&
                                    changes.get(i + before + 1).operation != Operation.EQUAL) {
                                    // never skip 1 line if showing before or after line is enabled.
                                    printBefore = true;
                                }
                            }
                        }

                        if (printBefore ||
                            printAfter-- > 0) {
                            if (skippedLines != 0) {
                                appendPatchLine(beforePatch,
                                                afterPatch,
                                                beforeComment,
                                                afterComment,
                                                builder,
                                                skippedLines,
                                                sourceLineNo,
                                                targetLineNo);
                                skippedLines = 0;
                            }

                            builder.append(beforeEq)
                                   .append(" ")
                                   .append(ch.text)
                                   .append(afterEq)
                                   .append('\n');
                        } else {
                            // Skip this line.
                            if (skippedLines < 1) {
                                // First line skipped, go from -1 to 1.
                                skippedLines = 1;
                            } else {
                                ++skippedLines;
                            }
                        }
                    }
                    break;
                }
                case DELETE: {
                    ++sourceLineNo;
                    if (skippedLines != 0) {
                        appendPatchLine(beforePatch,
                                        afterPatch,
                                        beforeComment,
                                        afterComment,
                                        builder,
                                        skippedLines,
                                        sourceLineNo,
                                        targetLineNo);
                        skippedLines = 0;
                    }
                    builder.append(beforeDel)
                           .append("-")
                           .append(ch.text)
                           .append(afterChange)
                           .append('\n');
                    printAfter = after;
                    printBefore = false;
                    break;
                }
                case INSERT: {
                    ++targetLineNo;
                    if (skippedLines != 0) {
                        appendPatchLine(beforePatch,
                                        afterPatch,
                                        beforeComment,
                                        afterComment,
                                        builder,
                                        skippedLines,
                                        sourceLineNo,
                                        targetLineNo);
                        skippedLines = 0;
                    }
                    builder.append(beforeAdd)
                           .append("+")
                           .append(ch.text)
                           .append(afterChange)
                           .append('\n');
                    printAfter = after;
                    printBefore = false;
                    break;
                }
            }
        }

        return builder.toString();
    }

    private void appendPatchLine(@Nonnull String beforePatch,
                                 @Nonnull String afterPatch,
                                 @Nonnull String beforeComment,
                                 @Nonnull String afterComment,
                                 StringBuilder builder,
                                 int skippedLines,
                                 int sourceLineNo,
                                 int targetLineNo) {
        builder.append(beforePatch)
               .append("@@ -")
               .append(sourceLineNo)
               .append(" +")
               .append(targetLineNo)
               .append(" @@")
               .append(afterPatch);
        if (skippedLines > 0) {
            builder.append(beforeComment)
                   .append(" -- (skipped ")
                   .append(skippedLines)
                   .append(" lines)")
                   .append(afterComment);
        }
        builder.append('\n');
    }

    private LinkedList<Change> makeLineDiff(String source, String target) {
        LinkedList<String> src_lines = new LinkedList<>();
        LinkedList<String> trg_lines = new LinkedList<>();
        Collections.addAll(src_lines, source.split("\\r?\\n"));
        Collections.addAll(trg_lines, target.split("\\r?\\n"));

        LinkedList<Change> beg = new LinkedList<>();
        LinkedList<Change> end = new LinkedList<>();
        while (true) {
            // This checks if the last change is a pure insert or delete.
            if (src_lines.isEmpty() || trg_lines.isEmpty()) {
                break;
            }

            // No change on all top lines -> beg (EQ).
            String src_first = src_lines.peekFirst();
            String trg_first = trg_lines.peekFirst();

            if (src_first.equals(trg_first)) {
                beg.add(new Change(Operation.EQUAL, src_lines.pollFirst()));
                trg_lines.pollFirst();
                continue;
            }

            // No change in bottom lines -> end (EQ)
            String src_last = src_lines.peekLast();
            String trg_last = trg_lines.peekLast();
            if (src_last.equals(trg_last)) {
                end.add(0, new Change(Operation.EQUAL, src_lines.pollLast()));
                trg_lines.pollLast();
                continue;
            }

            // a differing line.
            int up_next = src_lines.indexOf(trg_first);
            int down_next = trg_lines.indexOf(src_first);
            if (up_next == -1 && down_next >= 0) {
                // Added line.
                beg.add(new Change(Operation.INSERT, trg_lines.pollFirst()));
                continue;
            }
            if (down_next == -1 && up_next >= 0) {
                // Removed line.
                beg.add(new Change(Operation.DELETE, src_lines.pollFirst()));
                continue;
            }

            if (up_next >= 0 && down_next >= 0) {
                // Check number of lines moved **UP** (top in target found in source)
                int up_move = 1;
                while (up_next + up_move < src_lines.size() &&
                       up_move < trg_lines.size()) {
                    if (src_lines.get(up_next + up_move)
                              .equals(trg_lines.get(up_move))) {
                        ++up_move;
                    } else {
                        break;
                    }
                }

                // Check number of lines moved **DOWN** (top in source found in target)
                int down_move = 1;
                while (down_next + down_move < trg_lines.size() &&
                       down_move < src_lines.size()) {
                    if (trg_lines.get(down_next + down_move)
                              .equals(src_lines.get(down_move))) {
                        ++down_move;
                    } else {
                        break;
                    }
                }

                // First choose the shorter consecutive diff.
                if (up_move > down_move) {
                    up_move = 0;
                } else if (up_move < down_move) {
                    down_move = 0;
                } else {
                    // Then the closest diff.
                    if (up_next > down_next){
                        up_move = 0;
                    } else {
                        down_move = 0;
                    }
                }

                if (down_move > 0) {
                    while (down_move-- > 0) {
                        beg.add(new Change(Operation.DELETE, src_lines.pollFirst()));
                    }
                } else {
                    while (up_move-- > 0) {
                        beg.add(new Change(Operation.INSERT, trg_lines.pollFirst()));
                    }
                }
                continue;
            }

            // added AND removed, aka change.
            beg.add(new Change(Operation.DELETE, src_lines.pollFirst()));
            beg.add(new Change(Operation.INSERT, trg_lines.pollFirst()));
        }

        while (!src_lines.isEmpty()) {
            beg.add(new Change(Operation.DELETE, src_lines.pollFirst()));
        }
        while (!trg_lines.isEmpty()) {
            beg.add(new Change(Operation.INSERT, trg_lines.pollFirst()));
        }

        LinkedList<Change> changes = new LinkedList<>();
        changes.addAll(beg);
        changes.addAll(end);

        // Sort diff lines so that a continuous change of inserts and deletes becomes:
        // -- all deleted
        // -- all inserts
        LinkedList<Change> result = new LinkedList<>();
        // keep inserts for after deleted.
        LinkedList<Change> inserts = new LinkedList<>();
        for (Change change : changes) {
            switch (change.operation) {
                case EQUAL:
                    result.addAll(inserts);
                    inserts.clear();
                    result.add(change);
                    break;
                case DELETE:
                    result.add(change);
                    break;
                case INSERT:
                    inserts.add(change);
                    break;
            }
        }
        result.addAll(inserts);

        return result;
    }

    /**
     * @return The list of change and non-change entries, one per line.
     */
    @Override
    public LinkedList<Change> getChangeList() {
        return changeList;
    }
}
