---
layout: page
title: "About Diff-Util"
---

Utility classes for finding changes between strings. The main classes and uses
are:

- `Diff` finds diff between two strings. It is pretty advanced and can find minute
  differences in pretty long strings like data files etc. Returns a change set with
  equal, inserted and deleted parts of the text.
- `DiffLines` finds changes when comparing line-by-line only, not searching for changes
  within the lines themselves.
- `Bisect` is similar to `Diff`, but is limited to only using the `bisect` method, which
  makes it significantly faster when comparing smaller strings (less than a kilobyte),
  or content of files on very few very long lines.


# Releasing Diff-Util

Run the maven versions plugin to see what has been updated of dependencies and
plugins. See if updates should be done. Usually it's better to depend on
newer versions, as you may drag in older versions into other projects that
misses features or has specific bugs.

```bash
mvn versions:display-dependency-updates
mvn versions:display-plugin-updates
```

#### Making the release cut.

```bash
# Do the maven release:
mvn release:prepare
mvn release:perform
mvn release:clean
git fetch origin
```

If the artifacts found at the
[Nexus Repository Manager](https://oss.sonatype.org/#stagingRepositories)
are correct, you're ready to make the release. If not a git hard rollback is
needed (to remove release version, tag and commits). First make the actual
binary release:
